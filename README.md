# Dialog

Simple, therefore probably bad implementation of dialog window overlay. CSS mostly stolen from [Bootstrap](https://getbootstrap.com/) modal.

[Demo](https://edin.lockedin.gitlab.io/dialog/)

## Basic usage

You can initiate dialog with or without (with default) options. Options are:  

| option | default | accepts | description |
| ------ | ------- | ------- | ----------- | 
| large | `false` | truthy or falsy value | should dialog expand if view width allows it |
| z | `1050` | any number or string | set z-index, use exact number or set some string to ignore and always return default 1050 |
| remove | `true` | truthy or falsy value | whether to remove dialog from document after it's hidden |

```js
// default options
const basicDialog = Dialog()

// custom options
const customDialog = Dialog({ large: true, z: 1000, remove: false })
```

Dialog accepts either types of `string` or `node`. You can inject content with `inject` method:

```js
// inject string
const myDialog1 = basicDialog.inject('<p>Content of my first dialog</p>')

// inject node
const myNode = document.createElement('p')
myNode.textContent = 'Content of my second dialog'
const myDialog2 = customDialog.inject(myNode)
```

Inject method returns `node`. We show it with `show` method.

```js
document.getElementById('show1')
  .addEventListener(
    'mouseup',
    () => {
      basicDialog.show(myDialog1)
    },
    { passive: true, capture: false }
  )

document.getElementById('show2')
  .addEventListener(
    'mouseup',
    () => {
      customDialog.show(myDialog2)
    },
    { passive: true, capture: false }
  )

```

## Use on dialog already in document

You can use only show method if you already have your dialog statically set in html document. One way to do this is to use the skeleton method to get the empty dialog and add class: `_d-none` to top `div._dialog`. Put your content in `div._dialog-body` The skeleton with added classes should look something like this:

```html
<div class="_dialog _d-none" tabindex="-1" role="dialog" style="z-index: 1052">
    <button class="_dialog-close" type="button" data-dismiss="dialog" aria-label="Close" style="z-index: 1053"><span aria-hidden="true">×</span></button>
    <div class="_dialog-body" role="document">Some content...</div>
</div>

<button id="show3" type="button">Show existing</button>

<script>

const existingDialog = document.getElementById('existing-dialog')

document.getElementById('show3')
  .addEventListener(
    'mouseup',
    () => {
      customDialog.show(existingDialog)
    },
    { passive: true, capture: false }
  )

</script>
```

## Z-index

Notice the z-index style. Skeleton method checks for existing dialogs and adjusts z-index for overlap. This is neccessary only if you show more than one dialog at once. You can set `z` option to any string like 'skip' or 'ignore' to skip calculation and always set to value of `1050`. Or you can set your own value.

## Events

Showing and hidding dialogs emitts events similar to those on Bootstrap modals:  
- `dialog.showing`: when dialog appearing animation starts
- `dialog.shown`: when dialog is completed with showing transition
- `dialog.hiding`: when dialog starts to hide
- `dialog.hidden`: when dialog hidding animation stops
- `dialog.removed`: when dialog is removed from DOM


### TODO

- [accessibility](https://github.com/gdkraus/accessible-modal-dialog)
  - Esc key to close
  - focus element inside dialog
  - prevent focus to leave dialog
  - dialog title in aria-labelledby (first heading by speculation)
  - aria-describedby should contain usage instructions
  - these instructions could also be displayed somewhere outside dialog body
- error handling
- optimize css
- ~~better animations~~ ok for now
- jumpy when browser shows/hides no-overlay scrollbar
- grammar

# BUGS

## Removing from body  

~~Dialog should have some id because hide method removes dialog from document only based on attribute. If we have another dialog which we don't want to be removed it could remove it anyway.~~  
- resolved by querying closest dialog