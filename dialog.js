const Dialog = ({
  large = false,
  z = 1050,
  remove = true,
} = {}) => {
  const calcZ = z => {
    if (Number(z) !== 1050) {
      return Number(z)
    }
    if (isNaN(Number(z))) {
      return 1050
    }
    const dialogs = document.querySelectorAll('[role="dialog"]')
    if (dialogs.length > 0) {
      return 1050 + (dialogs.length * 2)
    }
    return 1050
  }
  
  const hidden = dialog => {
    const ev = new Event('dialog.hidden')
    dialog.dispatchEvent(ev)
  }
  const hiding = dialog => {
    const ev = new Event('dialog.hiding')
    dialog.dispatchEvent(ev)
  }
  const removed = dialog => {
    const ev = new Event('dialog.removed')
    dialog.dispatchEvent(ev)
  }
  const showing = dialog => {
    const ev = new Event('dialog.showing')
    dialog.dispatchEvent(ev)
  }
  const shown = dialog => {
    const ev = new Event('dialog.shown')
    dialog.dispatchEvent(ev)
  }
  
  const registerEvents = dialog => {
    showing(dialog)
    dialog.addEventListener('mouseup', hide, { passive: true, capture: false })
  }
  
  const s2h = s => document.createRange().createContextualFragment(s.trim())
  const skeleton = () => {
    if (!calcZ(z)) {
      return
    }
    return s2h(`
      <div class="_dialog" tabindex="-1" role="dialog" style="z-index: ${calcZ(z)}">
        <button class="_dialog-close" type="button" data-dismiss="dialog" aria-label="Close" style="z-index: ${calcZ(z) + 1}"><span aria-hidden="true">&times;</span></button>
        <div class="_dialog-body ${large && '_dialog-body-lg'}" role="document"></div>
      </div>
    `)
  }
  const convert = c =>
    c.nodeType > 0 ? c : s2h(c)

  const inject = content => {
    const fragment = skeleton()
    if (!fragment) {
      return
    }
    const node = fragment.firstChild
    node.querySelector('[role=document]').appendChild(convert(content))
    return node
  }
  
  const hide = e => {
    if (!(e.target.matches('._dialog') || e.target.matches('[data-dismiss=dialog]'))) {
      return
    }
    const dialog = e.target.closest('[role=dialog]')
    const dialogBody = dialog.querySelector('[role=document]')
    dialog.classList.remove('_fade-in')
    dialogBody.classList.remove('_drop-from-above')
    dialog.classList.add('_fade-out')
    dialogBody.classList.add('_pull-above')
    hiding(dialog)
    remove ?
      setTimeout(() => {
        dialog.remove()
        removed(dialog)
      }, 300) : 
      setTimeout(() => {
        dialog.classList.add('_d-none')
        hidden(dialog)
      }, 300)
    const body = document.querySelector('body')
    body.classList.remove('_dialog-open')
    return
  }
  
  const show = dialog => {
    if (!dialog) {
      return 'error'
    }
    if (!document.body.contains(dialog)) {
      document.querySelector('body').appendChild(dialog)    
    }
    registerEvents(dialog)
    const body = document.querySelector('body')
    body.classList.add('_dialog-open')
    const dialogBody = dialog.querySelector('[role=document]')
    dialog.classList.remove('_d-none', '_fade-out')
    dialogBody.classList.remove('_pull-above')
    dialog.classList.add('_fade-in')
    dialogBody.classList.add('_drop-from-above')
    setTimeout(() => {
      shown(dialog)
    }, 300)
    return 'success'
  }
  
  return {
    inject,
    show,
    skeleton: () => skeleton().firstChild,
  }
}